//
//  ProjectView.swift
//  throwAwayTest
//
//  Created by Endrit Buzi on 27.9.21.
//

import SwiftUI
class keepTrack: ObservableObject{
// klas e krujuar per te mbajtur variabla Bool per "isActive" attributem , ne menyre qe te deklarkohen si environment object dhe qe te njihen nga view te tjera
    @Published var variable : Bool = false
    
//    init(value:Bool){
//        self.variable = value
//    }
}
struct sheetView : View{
    @State private var projectName: String = " "
    @State private var taskDescription : String = ""
    @State private var tasks: [Task] = []
    var body: some View {
        VStack{
            Form{
                VStack{
                    Text("Project Title").font(Font.headline.bold())
                    TextField("Project Title",text : $projectName)
                    Spacer()
                    Text("Task description")
                    TextField("Leave empty for no active tasks",text : $taskDescription)
                    
                }
            }
            HStack{
                Button(action:{
                    tasks.append(Task(description: taskDescription, completed: false))
                    taskDescription = " "
                    
                }){
                    Text("Add Task").foregroundColor(.yellow).font(Font.headline.bold())
                }
                
                
                Button(action:{
                    postProject(project:Project(tasks: tasks, name: projectName ))
                    projectName = ""
                    
                
            }){
                Text("Create project").foregroundColor(.green).font(Font.headline.bold())
            }
                .padding()
                
            }
        }
    }
    
    
}
struct ProjectView: View {
    //@EnvironmentObject var projectt :Project
    @State var projects : [Project] = []
    @StateObject var taskView = keepTrack()
    @State var selection :UUID?
    @State var showSheet: Bool = false
    @State private var showAlert : Bool = false
    var body: some View {
        NavigationView{
            VStack{
            List(selection: $selection){
                
                ForEach(projects){
                    project in
                    Text("\(project.projectName)")
                }
 
            }
                HStack{
                    Button(action:{
                            
                                showSheet.toggle()
                                
                            
                    }){
                        
                        Text("ADD a new project").foregroundColor(.green).font(Font.headline.bold())
                    }
                    
                    
                    Spacer()
                    
                    Button(action:{
                        if selection != nil{
                            taskView.variable.toggle()
                            
                        }
                        else{
                            showAlert.toggle()
                        }
                    
                }){
                        Text("View tasks").foregroundColor(.yellow).font(Font.headline.bold())
                }}
                .padding()
                .onAppear{getProject(completion: { (project) in
                    self.projects = []
                    self.projects = project
                    })}
                NavigationLink(destination: ContentView(tasks: searchProject(id:unwrapUUID(id: selection)).tasks,projectUUID:unwrapUUID(id: selection)),isActive: $taskView.variable){
                    EmptyView()
                    NavigationLink(destination: sheetView(),isActive : $showSheet){
                        EmptyView()
                    }
                    //searchProject merr input uuid dhe shikon se kujt Project i perket ajo UUID ne projects.unwrapUUID thirret per te bere unwrap selection , e cila eshte deklaruar si conditional binding(per nje far arsye selection nuk mund ta benim dot force unwrapp pasi shaktonte nje bug ku nqs klikoje dy here ne te njejtin projekt app behej crash pasi vlera e selection behej nil).Aksesojme task-et e projektit te gjetur me ane te ".tasks" dhe ia bejme "feed" ContentView e cila merr si input nje list task-esh.
                    //Kalohet si input edhe projectUUID , per tu kaluar shkall shkall deri tek secondView ku perdoret si identifier per te   gjetur projektin ku duhet shtuar task-u.
                }
           
        }
            .navigationBarTitle(Text("Projects"))
            //.navigationBarBackButtonHidden(true)
            .toolbar{
                EditButton()
            }
    }
//        .sheet(isPresented:$showSheet){
//            sheetView()
//        }
        .environmentObject(taskView)
        .alert(isPresented: $showAlert){
            Alert(title: Text("No project was selected"), message: Text("Select a god damn project"), dismissButton: .default(Text("I promise ill select one next time ")))
        }
    }
    
//    func unwrappProject(project: [Project])->[Project]{
//
//    }

    func searchProject(id: UUID)->Project{
        for project in projects{
            if project.id == id {
                return project
            }
        }
        return Project(tasks: [], name: "Project was not found")
    }
    func unwrapUUID(id:UUID?) -> UUID{
        if let unwrapped = id {
            return unwrapped
        }
       return UUID()

    }
    
    
}

//
//struct ProjectView_Previews: PreviewProvider {
//    static var previews: some View {
//        ProjectView()
//    }
//}
