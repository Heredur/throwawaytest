//
//  project.swift
//  throwAwayTest
//
//  Created by Endrit Buzi on 27.9.21.
//

import Foundation
struct Project: Identifiable,Hashable,Equatable,Encodable,Decodable{
    static func == (lhs: Project, rhs: Project) -> Bool {
        return lhs.id == rhs.id
    }
    func hash(into hasher: inout Hasher) {
         hasher.combine(id)
         //hasher.combine(y)
     }
    
   
    
    var id: UUID
    var tasks : [Task] = []
    var projectName : String
    
    init(tasks : [Task] , name: String, uuid:UUID = UUID()){
        
        self.tasks = tasks
        projectName = name
        self.id = uuid
    }
    
}
let testProjects = [
    Project(tasks:testTasks, name: "1"),
    Project(tasks:testTasks, name: "2"),
    Project(tasks:testTasks, name: "3"),
    Project(tasks:testTasks, name: "4"),
]
func postProject(project: Project){
    
    guard let encoded = try? JSONEncoder().encode(project)
    else  {
        print("Error when trying to encode the data , ofc  i dont know what happened so im just throwing this out there")
        return 
    }
    
    let url = URL(string: "http://127.0.0.1:8000/projects/")
    
    var request = URLRequest(url: url!)
    
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "POST"
    request.httpBody = encoded
    URLSession.shared.dataTask(with:request){
        data, response , error in
        guard data != nil else{
            print("no data in response \(error?.localizedDescription ?? "unknown error")")
            return
                
        }
        if response == nil{
            print("No response for some reason")
        } else {
            print(response!)
            return
            
        }
    }
    .resume()
    
}

func getProject(completion: @escaping ([Project]) -> ()){
    guard let url = URL(string:"http://127.0.0.1:8000/projects/") else {
        print("Api is down")
        return
    }
   var request = URLRequest(url: url)
//
//    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
   request.httpMethod = "GET"
    
    URLSession.shared.dataTask(with: request) { data, response, _ in
        let projects = try! JSONDecoder().decode([Project].self, from: data!)
        DispatchQueue.main.async {
            completion(projects)
        }
               }
    .resume()
    
}

func addTaskToProject(project : Project){
    guard let encoded = try? JSONEncoder().encode(project)
    else  {
        print("Error when trying to encode the data , ofc  i dont know what happened so im just throwing this out there")
        return
    }
    
    let url = URL(string: "http://127.0.0.1:8000/projects/")
    
    var request = URLRequest(url: url!)
    
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "PUT"
    request.httpBody = encoded
    
    URLSession.shared.dataTask(with:request){
        data, response , error in
        guard data != nil else{
            print("no data in response \(error?.localizedDescription ?? "unknown error")")
            return
                
        }
        if response == nil{
            print("No response for some reason")
        } else {
            print(response!)
            return
            
        }
    }
    .resume()
    

    
}
