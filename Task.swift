//
//  Task.swift
//  ToDoDjango
//
//  Created by Endrit Buzi on 22.9.21.
//

import Foundation
//
//  Task.swift
//  ToDoList
//
//  Created by Endrit Buzi on 20.9.21.
//

import Foundation
struct Task : Identifiable,Hashable,Decodable,Encodable{
    var id :UUID
    var description : String
    var completed : Bool
    
    init(id:UUID = UUID() , description : String , completed : Bool) {
        self.id = id
        self.description = description
        self.completed = completed
    }
}

let testTasks = [
    
    Task(description: "This is task number 1", completed: false),
    Task(description: "Task number 2", completed: true),
    Task(description: "3 ", completed: false),
    Task(description: "4 ", completed: true),
        
]
class Api {
    func getTasks(completion: @escaping ([Task]) -> ()) {
        guard let url = URL(string:"http://127.0.0.1:8000/tasks/") else {
            print("Api is down")
            return
        }

        URLSession.shared.dataTask(with: url) { data, _, _ in
            let tasks = try! JSONDecoder().decode([Task].self, from: data!)
            DispatchQueue.main.async {
                completion(tasks)
            }
                   }
        .resume()
    }
    
//    func getTasks() -> [Task]{
//        let url = URL(string:"http://127.0.0.1:8000/tasks/")!
//
//        var request  = URLRequest(url: url)
//        request.httpMethod = "GET"
//        URLSession.shared.dataTask(with: request){
//            data ,_ ,_ in
//            let tasks = try! JSONDecoder().decode([Task].self,from: data!)
//
//        }
//
//            .resume()
//    }
    
    func postTask(task:Task){
        guard let encoded = try? JSONEncoder().encode(task)
        else{
            print("failed to enocde order")
            return
        }
        
        let url = URL(string:"http://127.0.0.1:8000/tasks/")!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = encoded
        
        URLSession.shared.dataTask(with: request){
            data , response , error in
            guard data != nil else{
                print("no data in response \(error?.localizedDescription ?? "unknown error")")
                return
            }
            
        }.resume()
    }
    
    func UpdateTask(task:Task){
        guard let encoded = try? JSONEncoder().encode(task)
        else  {
            print("Error when trying to encode the data , ofc  i dont know what happened so im just throwing this out there")
            return
        }
        let url = URL(string: "http://127.0.0.1:8000/tasks/")
        
        var request = URLRequest(url: url!)
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "PUT"
        request.httpBody = encoded
        URLSession.shared.dataTask(with:request){
            data, response , error in
            guard data != nil else{
                print("no data in response \(error?.localizedDescription ?? "unknown error")")
                return
                    
            }
            if response == nil{
                print("No response for some reason")
            } else {
                print(response!)
                return
                
            }
        }
        .resume()
        
        
        
      }
        }
        



