//
//  throwAwayTestApp.swift
//  throwAwayTest
//
//  Created by Endrit Buzi on 22.9.21.
//

import SwiftUI

@main
struct throwAwayTestApp: App {
    var body: some Scene {
        WindowGroup {
            ProjectView()
        }
    }
}
