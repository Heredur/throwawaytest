//
//  ContentView.swift
//  throwAwayTest
//
//  Created by Endrit Buzi on 22.9.21.
//

import SwiftUI

struct ContentView: View {
    //@State private var testTask = Task(description: "?????", completed: false)
    @State var tasks: [Task]
    //@State private var showAlert : Bool = false
    @State var projectUUID: UUID 
    @State private var selection :UUID?
    @State private var showSearchValue :Bool = false
    @State private var nextView : Bool = false
    @EnvironmentObject var taskView: keepTrack
    var body: some View {
        NavigationView{
            
        VStack{
            //Form{
            List(selection: $selection){
            ForEach(tasks){
            task in
            HStack{
                Image(systemName: task.completed ? "checkmark.circle.fill" : "circle")
            Text(task.description)
            }
        }
//           .onDelete{
//              IndexSet in
//                tasks.remove(atOffsets: IndexSet)            }

        }
                
        //}
            .padding()
//            .onAppear{Api().getTasks(completion: { (task) in
//                self.tasks = task
//                })}
            NavigationLink(destination: secondView(taskss: tasks,projectUUID: projectUUID),isActive: $nextView){
                EmptyView()
            }
            HStack{
            Button(action:{
                nextView.toggle()
            }){
                HStack{
                    Image(systemName: "plus.circle.fill")
                    Text("Add another task").foregroundColor(.green)
                }
                }
            .padding()
            .accentColor(.green)
            
                Button(action:{
                    if let unwrapped = selection{
                        let taskToUpdate = findTask(unwrapped)
                        Api().UpdateTask(task: taskToUpdate)
                        taskView.variable.toggle()
                        
                    }
                }){
                    HStack{
                        Image(systemName: "checkmark.circle.fill")
                        Text("Mark as complete").foregroundColor(.red)
                    }
                }
                .padding()
                .accentColor(.red)
            }
            
            if let selectionText = selection{
                Text("\(selectionText.description)")
            }
            //Text("hi")
        }
       
        .navigationBarTitle(Text("List of Tasks"))
        .navigationBarBackButtonHidden(true)
        .toolbar{
                EditButton()

        }
        }
        
        
}
    func findTask( _ id : UUID) ->Task {
        for item in tasks {
            if item.id == id {
                
                return Task(id: item.id, description: item.description, completed: true)
            }
        }
        return Task(description: "Task wasnt found", completed: false)
    }
    
}
   

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(tasks: [],projectUUID: UUID() )
    }
}

